﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestProject
{

	public class UIMainMenu : MonoBehaviour
	{

		public void OnClick_PlayBtn()
		{
			GameController.GotoGame();
		}

		public void OnClick_QuitBtn()
		{
			GameController.Quit();
		}
	}
}