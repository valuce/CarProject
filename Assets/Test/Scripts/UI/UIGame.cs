﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestProject
{

	public class UIGame : MonoBehaviour
	{
		[SerializeField]
		private GameObject _pausePanal;

		private void Awake()
		{
			if( _pausePanal )
				_pausePanal.SetActive( false );
		}

		private void OnEnable()
		{
			Notificator.onPauseGame += OnPauseGame;
			Notificator.onResumeGame += OnResumeGame;
		}

		private void OnDisable()
		{
			Notificator.onResumeGame -= OnResumeGame;
			Notificator.onPauseGame -= OnPauseGame;
		}


		private void OnPauseGame()
		{
			if( _pausePanal )
				_pausePanal.SetActive( true );
		}

		private void OnResumeGame()
		{
			if( _pausePanal )
				_pausePanal.SetActive( false );
		}

		public void OnClick_PauseBtn()
		{
			GameController.PauseGame( true );
		}

		public void OnClick_ResumeBtn()
		{
			GameController.PauseGame( false );
		}

		public void OnClick_QuitBtn()
		{
			GameController.Quit();
		}

		public void OnClick_BackBtn()
		{
			GameController.GotoMainMenu();
		}

		public void OnClick_RestartBtn()
		{
			GameController.ReloadeScene();
		}
	}
}