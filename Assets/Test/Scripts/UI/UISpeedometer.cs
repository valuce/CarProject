﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Vehicles.Car;

namespace TestProject
{

	public class UISpeedometer : MonoBehaviour
	{
		[SerializeField]
		private Text _speedText;
		[SerializeField]
		private Transform _arrow;

		private CarController _car;

		private void OnEnable()
		{
			Notificator.onCreatePlayer += Init;
		}

		private void OnDisable()
		{
			Notificator.onCreatePlayer -= Init;
		}

		public void Init( GameObject player )
		{
			_car = player.GetComponent<CarController>();
		}

		private void LateUpdate()
		{
			if( _car == null )
				return;

			if( _speedText )
				_speedText.text = ((int)_car.CurrentSpeed).ToString() + " " + _car.speedTypeStr;

			if( _arrow )
			{
				float angle = Mathf.Lerp( 0, -180, _car.CurrentSpeed / _car.MaxSpeed );
				_arrow.localEulerAngles = Vector3.forward * angle;
			}
		}
	}
}
