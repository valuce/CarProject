﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestProject
{

	public class TimedObjectDestructor : MonoBehaviour
	{

		[SerializeField] private float _timeOut = 1.0f;

		private void Start()
		{
			Invoke( "DestroyNow", _timeOut );
		}

		private void OnDisable()
		{
			CancelInvoke();
		}

		private void DestroyNow()
		{
			GameObjectsPool.Destroy( gameObject );
		}
	}
}