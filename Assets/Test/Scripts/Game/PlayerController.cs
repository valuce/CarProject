﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Vehicles.Car;

namespace TestProject
{

	public class PlayerController : MonoBehaviour
	{
		private Weapon _weapon;		
		private CarController _car; // the car controller we want to use

		public void SetPawn( GameObject pawn )
		{
			_car = pawn.GetComponent<CarController>();
			_weapon = pawn.GetComponentInChildren<Weapon>();
		}

		private void FixedUpdate()
		{
			if( GameController.isPause )
				return;

			if( _car == null || !_car.isActiveAndEnabled )
				return;


			float h = CrossPlatformInputManager.GetAxis( "Horizontal" );
			float v = CrossPlatformInputManager.GetAxis( "Vertical" );
#if MOBILE_INPUT
			_car.Move( h, v, v, 0f );
#else
			float handbrake = CrossPlatformInputManager.GetAxis( "Jump" );
			_car.Move( h, v, v, handbrake );
#endif
		}

		private void LateUpdate()
		{
			if( GameController.isPause )
				return;

			if( CrossPlatformInputManager.GetButtonDown( "Fire1" ) )
			{
				if( _weapon )
					_weapon.Shoot();

				Debug.Log( "Fire" );
			}
		}
	}
}