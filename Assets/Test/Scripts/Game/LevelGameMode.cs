﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestProject
{

	public class LevelGameMode : MonoBehaviour
	{
		[SerializeField]
		private PlayerController playerController;
		[SerializeField]
		private GameObject Pawn;
		[SerializeField]
		private Transform _playerStart;
		[SerializeField]
		private GameObject ShapePrefab;
		[SerializeField]
		private Transform _dynamicParent;

		private void Start()
		{
			var pawn = Instantiate( Pawn, _playerStart.position, _playerStart.rotation, transform.parent );
			pawn.name = Pawn.name;
			Notificator.CreatePlayer( pawn );

			var controller = Instantiate( playerController, transform.parent );
			controller.name = playerController.name;
			controller.SetPawn( pawn );

			SpawnShapes( 20, 200f );
		}


		private void SpawnShapes( int count, float size )
		{
			for( int i = 0; i < count; ++i )
			{
				Vector3 position = new Vector3( Random.Range( -size, size ), 0f, Random.Range( -size, size ) );
				Quaternion rotation = Quaternion.Euler( 0, Random.value * 360f, 0 );

				Instantiate( ShapePrefab, position, rotation, _dynamicParent );
			}
		}
	}
}