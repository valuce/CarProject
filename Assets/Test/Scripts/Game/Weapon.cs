﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestProject
{

	public class Weapon : MonoBehaviour
	{
		[SerializeField]
		private GameObject BulletPrefab;
		[SerializeField]
		private Transform _muzzle;

		private AudioSource _audioSource;

		[SerializeField]
		private float _shootDelay = 0.2f;
		private float _timer;

		private void Awake()
		{
			_audioSource = GetComponent<AudioSource>();
		}

		private void Update()
		{
			_timer -= Time.deltaTime;			
		}

		public bool Shoot()
		{
			if( _timer > 0 )
				return false;

			GameObjectsPool.Instantiate( BulletPrefab, _muzzle.position, _muzzle.rotation );

			if( _audioSource )
				_audioSource.Play();

			_timer = _shootDelay;

			return true;
		}
	}
}