﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestProject
{
	[RequireComponent( typeof( Rigidbody ) )]
	public class Bullet : MonoBehaviour
	{
		private Rigidbody _rigidbody;

		[SerializeField]
		private float _force;
		[SerializeField]
		private GameObject ExplosionPrefab;

		private void Awake()
		{
			_rigidbody = GetComponent<Rigidbody>();
		}

		private void Start()
		{
			_rigidbody.velocity = Vector3.zero;
			_rigidbody.angularVelocity = Vector3.zero;
			_rigidbody.AddForce( transform.forward * _force, ForceMode.Impulse );
		}

		private void OnCollisionEnter( Collision collision )
		{
			GameObjectsPool.Instantiate( ExplosionPrefab, transform.position, transform.rotation );

			GameObjectsPool.Destroy( gameObject );
		}
	}
}