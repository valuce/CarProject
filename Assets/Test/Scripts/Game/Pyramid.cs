﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestProject
{

	public class Pyramid : MonoBehaviour
	{
		[SerializeField]
		private GameObject PartPrefab;
		[SerializeField]
		private float _size = 1f;
		[SerializeField]
		private int _heightMin = 4;
		[SerializeField]
		private int _heightMax = 6;
		[SerializeField]
		private float _checkRebuildTime = 1f;

		private List<Vector3> _pattern = new List<Vector3>();		

		private void Start()
		{
			int height = Random.Range( _heightMin, _heightMax + 1 );
			_pattern = GeneratePyramidPattern( height, _size );
			CheckValidParts( _pattern.Count );			
			Build( _pattern );

			InvokeRepeating( "CheckRebuild", _checkRebuildTime, _checkRebuildTime );
		}

		private void CheckRebuild()
		{
			if( CanRebuild() && !IsVisble() )
			{
				Debug.Log( "Rebuild" );
				Build( _pattern );
			}
		}		

		private bool IsVisble()
		{
			foreach( Transform child in transform )
			{
				Renderer renderer = child.GetComponent<Renderer>();
				if( renderer && renderer.isVisible )
					return true;
			}
			return false;
		}

		private List<Vector3> GeneratePyramidPattern( int height, float size )
		{
			Vector2 offset = Vector2.one * size * 0.5f;
			List<Vector3> pattern = new List<Vector3>();

			for( int i = height; i > 0; --i )
			{
				float halfI = (i * 0.5f);
				float y = height - i;
				for( int j = 0; j < i; ++j )
				{
					float x = j - halfI;
					pattern.Add( new Vector2(x, y) + offset );
				}
			}

			return pattern;
		}

		private void CheckValidParts( int count )
		{
			int childCount = transform.childCount;			
			int countMax = Mathf.Max( count, childCount );

			Transform child;
			for( int i = 0; i < countMax; ++i )
			{
				if( i >= childCount )
				{
					child = Instantiate( PartPrefab, transform ).transform;
				}
				else
				{
					child = transform.GetChild( i );
				}

				child.gameObject.SetActive( i < count );
			}
		}

		private void Build( List<Vector3> pattern )
		{

			for( int i = 0; i < pattern.Count; ++i )
			{
				Transform part = transform.GetChild(i);

				Rigidbody rigidbody = part.GetComponent<Rigidbody>();
				if( rigidbody )
				{
					rigidbody.velocity = Vector3.zero;
					rigidbody.angularVelocity = Vector3.zero;
				}
				
				part.localPosition = pattern[i];
				part.localRotation = Quaternion.identity;
			}
		}

		private bool CanRebuild()
		{
			if( _pattern.Count == 0 || _pattern.Count != transform.childCount )
				return false;

			Vector3 pos = _pattern[_pattern.Count - 1];
			Transform part = transform.GetChild( _pattern.Count - 1 );
			
			return Vector3.Distance(pos, part.localPosition) > 0.01f;
		}
	}
}