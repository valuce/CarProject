﻿using UnityEngine;
using System.Collections;

namespace TestProject
{

	public static class Notificator
	{
		static public event System.Action<GameObject> onCreatePlayer;
		static public event System.Action onPauseGame;
		static public event System.Action onResumeGame;



		static public void CreatePlayer( GameObject player )
		{
			if( onCreatePlayer != null )
				onCreatePlayer.Invoke( player );
		}

		static public void PauseGame( bool pause )
		{
			if( pause )
			{
				if( onPauseGame != null )
					onPauseGame.Invoke();
			}
			else
			{
				if( onResumeGame != null )
					onResumeGame.Invoke();
			}
		}
	}
}