﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TestProject
{
	public enum GameState
	{
		None, MainMenu, Game,
	}

	public class GameController : MonoBehaviour
	{
		public static GameController Inst { private set; get; }

		private void Awake()
		{
			if( Inst != null )
			{
				Destroy( gameObject );
				return;
			}

			Inst = this;
			DontDestroyOnLoad( gameObject );

			Application.targetFrameRate = 60;
		}

		private void OnApplicationPause( bool pause )
		{
			if( !_isPause && pause )
				PauseGame( pause );
		}

		//STATIC
		static private bool _isPause = false;

		static public bool isPause { get { return _isPause; } }

		public static void PauseGame( bool pause )
		{
			if( pause == _isPause )
				return;

			_isPause = pause;

			Time.timeScale = _isPause ? 0 : 1;
			AudioListener.pause = _isPause;

			Notificator.PauseGame( _isPause );
		}

		public static void GotoMainMenu()
		{
			GotoScene( "MainMenu" );
		}

		public static void GotoGame()
		{
			GotoScene( "GameScene" );
		}

		private static void GotoScene( string sceneName )
		{
			SceneManager.LoadScene( sceneName );
			PauseGame( false );
		}

		public static void ReloadeScene()
		{
			GotoScene( SceneManager.GetActiveScene().name );
		}

		public static void Quit()
		{
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#else
			Application.Quit();
#endif
		}
	}
}